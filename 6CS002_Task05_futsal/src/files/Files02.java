package files;

import java.io.*;

/******************************************************************************
 * This program counts the number of lines in a text file.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Files02 {

  public static void main(String[] args) throws Exception {
    BufferedReader r  = 
      new BufferedReader(new FileReader("G://CINEC//3rd year//1st sem//Advanced Software Engineering Topics - CS0002//Files/wolf-fox.txt"));

    System.out.println(r.lines().count());

    r.close();
  }

}


