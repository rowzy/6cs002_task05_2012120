package futsal;

import java.util.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class futsal04 {
  public static void main(String[] args) {
    List<Club> table = Arrays.asList(
    		new Club(1, "Cinec Girls", 6, 5, 1, 5, 61, 400, 223, 45, 41, 9, 4, 96),
	        new Club(2, "Salmaan's Dolls", 6, 6, 0, 6, 62, 418, 311, 72, 43, 10, 3, 75),
	        new Club(3, "Pearl Island", 6, 4, 1, 6, 53, 621, 32, 37, 39, 4, 2, 68),
	        new Club(4, "IIT Blues", 6, 6, 1, 5, 64, 218, 346, 70, 40, 5, 5, 68),
	        new Club(5, "Seychelles", 6, 6, 0, 6, 66, 837, 226, 70, 46, 5, 7, 98),
	        new Club(6, "Lio", 6, 4, 2, 1, 72, 727, 245, 77, 54, 9, 4, 61),
	        new Club(7, "Mode Sharks", 6, 4, 0, 1, 97, 1082, 15, 62, 54, 6, 0, 56),
	        new Club(8, "Queens", 6, 3, 0, 2, 44, 114, -80, 45, 50, 4, 5, 49),
	        new Club(9, "Rotaract", 6, 4, 1, 2, 55, 975, -32, 53, 61, 4, 6, 48),
	        new Club(10, "Mid Town Bisons", 6, 2, 1, 4, 44, 478, -236, 46, 17, 4, 0, 40),
	        new Club(11, "APIIT Blacks", 6, 1, 1, 6, 75, 445, -90, 57, 61, 4, 8, 64),
	        new Club(12, "PanColombo", 6, 0, 0, 2, 23, 1221, -998, 29, 147, 1, 0, 1));
    
    System.out.println("Several clubs have 68 points:\n");
    System.out.println("   Club Name               pointsFor        P.Against      points\n"); 
    table.stream().filter(club -> club.getPoints() == 68)
        .forEach(System.out::println);
    System.out.println("==================================================================================");
    
    System.out.println();
    System.out.println("Clubs with 56 points:\n");
    System.out.println("   Club Name               pointsFor        P.Against      points\n"); 
    table.stream().filter(club -> club.getPoints() == 56)
        .forEach(System.out::println);
    System.out.println("==================================================================================");
    
    System.out.println();
    System.out.println("Clubs who won less than 10 matches:\n");
    System.out.println("   Club Name               pointsFor        P.Against      points\n"); 
    table.stream().filter(SoccerClub -> SoccerClub.getWon() < 10)
        .forEach(System.out::println);
    System.out.println("==================================================================================");
    
    System.out.println();
    System.out.println("Clubs who won more than 2 matches:\n");
    System.out.println("   Club Name               pointsFor        P.Against      points\n"); 
    table.stream().filter(SoccerClub -> SoccerClub.getWon() > 2)
        .forEach(System.out::println);
    
    try {
	      FileWriter writer = new FileWriter("futsal04.txt");
	      writer.write("Several clubs have 68 points:\n");
	      writer.write("------------------------------\n\n");
	      writer.write("   club Name                   pointsFor      P.Against     points\n");
	      writer.write("   ---------                   ---------      ---------     ------\n");
	      table.stream().filter(FutsalClub -> FutsalClub.getWon() > 8)
	        .forEach(str -> {
	        	try {
	        		writer.write(str.toString() + "\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
	        });
	      
	      writer.write("\nClubs with 56 points:\n");
	      writer.write("------------------------------\n\n");
	      writer.write("   club Name                   pointsFor      P.Against     points\n");
	      writer.write("   ---------                   ---------      ---------     ------\n");
	      table.stream().filter(FutsalClub -> FutsalClub.getLost() > 5)
	        .forEach(str -> {
	        	try {
	        		writer.write(str.toString() + "\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
	        });
	      
	      writer.write("\nClubs who won less than 10 matches:\n");
	      writer.write("------------------------------\n\n");
	      writer.write("   club Name                   pointsFor      P.Against     points\n");
	      writer.write("   ---------                   ---------      ---------     ------\n");
	      table.stream().filter(FutsalClub -> FutsalClub.getLost() > 5)
	        .forEach(str -> {
	        	try {
	        		writer.write(str.toString() + "\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
	        });
	      
	      writer.write("\nClubs who won more than 2 matches:\n");
	      writer.write("------------------------------\n\n");
	      writer.write("   club Name                   pointsFor      P.Against     points\n");
	      writer.write("   ---------                   ---------      ---------     ------\n");
	      table.stream().filter(FutsalClub -> FutsalClub.getLost() > 5)
	        .forEach(str -> {
	        	try {
	        		writer.write(str.toString() + "\n");
				} catch (IOException e) {
					e.printStackTrace();
				}
	        });
	      
	      writer.close();
	      System.out.println("\nSuccessfully wrote to the file futsal04.txt.");
	    } catch (IOException e) {
	      System.out.println("An error occurred.");
	      e.printStackTrace();
	    }
}


  }


