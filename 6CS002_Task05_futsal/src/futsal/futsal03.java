package futsal;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;

public class futsal03 {
  public static void main(String[] args) {
    List<Club> table = Arrays.asList(
    		new Club(1, "Cinec Girls", 6, 5, 1, 5, 61, 400, 223, 45, 41, 9, 4, 96),
	        new Club(2, "Salmaan's Dolls", 6, 6, 0, 6, 62, 418, 311, 72, 43, 10, 3, 75),
	        new Club(3, "Pearl Island", 6, 4, 1, 6, 53, 621, 32, 37, 39, 4, 2, 68),
	        new Club(4, "IIT Blues", 6, 6, 1, 5, 64, 218, 346, 70, 40, 5, 5, 68),
	        new Club(5, "Seychelles", 6, 6, 0, 6, 66, 837, 226, 70, 46, 5, 7, 98),
	        new Club(6, "Lio", 6, 4, 2, 1, 72, 727, 245, 77, 54, 9, 4, 61),
	        new Club(7, "Mode Sharks", 6, 4, 0, 1, 97, 1082, 15, 62, 54, 6, 0, 56),
	        new Club(8, "Queens", 6, 3, 0, 2, 44, 114, -80, 45, 50, 4, 5, 49),
	        new Club(9, "Rotaract", 6, 4, 1, 2, 55, 975, -32, 53, 61, 4, 6, 48),
	        new Club(10, "Mid Town Bisons", 6, 2, 1, 4, 44, 478, -236, 46, 17, 4, 0, 40),
	        new Club(11, "APIIT Blacks", 6, 1, 1, 6, 75, 445, -90, 57, 61, 4, 8, 64),
	        new Club(12, "PanColombo", 6, 0, 0, 2, 23, 1221, -998, 29, 147, 1, 0, 1));
    
    OptionalInt min = table.stream().mapToInt(Club::getPoints).min();
    if (min.isPresent()) {
      System.out.printf("Lowest number of points is: %d\n", min.getAsInt());
    } else {
      System.out.println("min failed");
    }
    
    OptionalInt max = table.stream().mapToInt(Club::getPoints).max();
    if (max.isPresent()) {
    	System.out.printf("Highest number of points is: %d\n", max.getAsInt());
    } else {
    	System.out.println("max failed");
    }
    
    // reduce
    System.out.println("Total win: ");
    Integer output = table.stream().map(Club::getWon).reduce(0, (a, b) -> a + b);
    System.out.println(output);
    
    //Collectors
    List<String> result = table.stream().filter(p -> p.getWon() > 5).map(Club::getClub).collect(Collectors.toList());
    System.out.println(result.toString());
    
    try {
	      FileWriter writer = new FileWriter("futsal03.txt");
	      
	      writer.write("Lowest number of points is: " + min.getAsInt() + "\n");	     
	      writer.write("Highest number of points is: " + max.getAsInt() + "\n");
	      writer.write("Total win:\n " + result + "\n");
	      writer.close();
	      System.out.println("\nSuccessfully wrote to the file futsal03.txt.");
	    } catch (IOException e) {
	      System.out.println("An error occurred.");
	      e.printStackTrace();
	    }
}

  }



