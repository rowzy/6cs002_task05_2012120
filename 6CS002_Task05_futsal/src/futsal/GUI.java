package futsal;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class GUI extends JFrame {
private JPanel contentPane;
	
	public GUI() {
		setResizable(false);
		setTitle("2012120_6CS002_Task05");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(600, 600);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel title = new JLabel("<html><h1><strong><i>Futsal League 23'</i></strong></h1><hr></html>");
		title.setBounds(210, 34, 45, 16);
		title.resize(200, 50);
		contentPane.add(title);
		
		JButton league1 = new JButton("Fustsal League 01");
		league1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUI.main(null);
			}
		});
		league1.setBounds(200, 80, 117, 29);
		league1.resize(200, 50);
		contentPane.add(league1);
		
		JButton league2 = new JButton("Fustsal League 02");
		league2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUI.main(null);
			}
		});
		league2.setBounds(200, 150, 117, 29);
		league2.resize(200, 50);
		contentPane.add(league2);
		
		JButton league3 = new JButton("Fustsal League 03");
		league3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUI.main(null);
			}
		});
		league3.setBounds(200, 220, 117, 29);
		league3.resize(200, 50);
		contentPane.add(league3);
		
		JButton league4 = new JButton("Fustsal League 04");
		league4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUI.main(null);
			}
		});
		league4.setBounds(200, 290, 117, 29);
		league4.resize(200, 50);
		contentPane.add(league4);
		
		JButton league5 = new JButton("Fustsal League 05");
		league5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUI.main(null);
			}
		});
		league5.setBounds(200, 360, 117, 29);
		league5.resize(200, 50);
		contentPane.add(league5);
		
		JButton exit = new JButton("Exit");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		exit.setBounds(200, 440, 117, 29);
		exit.resize(200, 50);
		contentPane.add(exit);
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
